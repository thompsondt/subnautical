# SubnautiCAL - The Subnautica Position Calculator

Use two or three range measurements to calculate the position of a point of interest (usually, you).

## Introduction
As your galaxy-class space ship (the Aurora) is crashing towards Planet 4546B, you find yourself racing towards your escape pod. You eject from the ship just in time, but still barely survive your fiery descent to the planet below. Emerging from your escape pod you now find yourself on a watery, ocean planet with very few tools immediately avaialble for your survival. To find precious resources and avoid dangerous hazards, good navigation is essential. Being the highly intelligent creature that you are, you decide to map your surroundings.

Despite Alterran technology being quite advanced, you find your (inexplicably crude) compass rather slow and tedious to use for cartographic purposes. This is where the Subnautica Position Calculator becomes handy. It uses [True-Range Multilateration](https://en.wikipedia.org/wiki/True-range_multilateration) to determine your location using range measurments to fixed positions. You may use the distance to your life pod as one such point of reference. Add in at least one beacon placed at a known distance from your life pod and you can begin mapping and surveying your new home while you survive and wait for rescue.

## Usage
The program assumes that you have two stations for reference:
* Station 1 - the life pod at x=0, y=0
* Station 2 - a station called "whiskey base" at x=-500, y=0 (marked by a beacon)

The program also assumes that everything exists in a flat plane.

## To Do
* Allow users manage their own stations
* Calculate in three dimensions