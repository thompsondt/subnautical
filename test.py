#!/bin/python3

import unittest
from subnautical import multilateration 

class TestTwoDimensionReadings(unittest.TestCase):
	def setUp(self):
		self.station_a = {'x': 0, 'y': 0}
		self.station_b = {'x': -4, 'y': 0}

	def test_station_b_plus_three_y(self):
		actual_position = {'x': -4.0, 'y': 3.0, 'z': None}
		position = multilateration.resolve(
			5, self.station_a,
			3, self.station_b
		)
		self.assertEqual(actual_position, position)
		
	def test_station_b_plus_three_y__reverse_input(self):
		# Tests a situation where we're using station a as the secondary point, which could produce a "division by zero" error if not handled correctly.
		actual_position = {'x': -4.0, 'y': 3.0, 'z': None}
		position = multilateration.resolve(
			3, self.station_b,
			5, self.station_a
		)
		self.assertEqual(actual_position, position)

	def test_non_intersecting_radials(self):
		# Tests a situation where the distances to two stations are so small, that no intersecting radials are produced.
		with self.assertRaises(multilateration.ImpossiblePositionError):
			# These stations are further away than the readings make possible
			multilateration.resolve(
				0.5, self.station_a,
				0.5, self.station_b
			)

class TestDepthAwareReadings(unittest.TestCase):
	def setUp(self):
		self.station_a = {'x': 0, 'y': 0, 'z': 0}
		self.station_b = {'x': -4, 'y': 0, 'z': 0}
		self.station_c = {'x': -4, 'y': 0, 'z': -10}
		self.actual_position = {'x': 15, 'y': -12, 'z': -12}
		self.station_a_distance = 22.649503
		self.station_b_distance = 25.475478
		self.station_c_distance = 22.561028

	def test_resolve_3d_position_against_a_and_b(self):
		position = multilateration.resolve(
			self.station_a_distance, self.station_a,
			self.station_b_distance, self.station_b,
			depth=self.actual_position['z'])
		for key in ["x", "y", "z"]:
			try:
				self.assertAlmostEqual(self.actual_position[key], position[key], 3)
			except AssertionError:
				# Cover scenarios where y axis is ambiguous
				self.assertAlmostEqual(self.actual_position[key] * -1, position[key], 3)

	def test_resolve_3d_position_against_a_and_c(self):
		position = multilateration.resolve(
			self.station_a_distance, self.station_a,
			self.station_c_distance, self.station_c,
			depth=self.actual_position['z'])
		for key in ["x", "y", "z"]:
			try:
				self.assertAlmostEqual(self.actual_position[key], position[key], 3)
			except AssertionError:
				# Cover scenarios where y axis is ambiguous
				self.assertAlmostEqual(self.actual_position[key] * -1, position[key], 3)

if __name__ == '__main__':
	unittest.main()