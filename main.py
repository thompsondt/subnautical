#!/bin/python3
from os import system, name
import logging
import subnautical.multilateration as subnautical

logging.basicConfig(filename='subnautical.log', filemode='a', level=logging.DEBUG)

# Setup Fixed Stations
logging.debug('Configuring Stations')

station_a = {
	'name': 'Life pod',
	'x': 0,
	'y': 0,
	'z': 0
}

station_b = {
	'name': 'Whiskey Base',
	'x': -500,
	'y': 0,
	'z': -40 
}

logging.debug('Begin main loop')

while True:
	if name == 'nt': #if windows
		_ = system('cls')
	else:
		_ = system('clear')

	# Gather input
	station_a_distance = float(input(f"Distance to {station_a['name']}: "))
	station_b_distance = float(input(f"Distance to {station_b['name']}: "))
	depth = float(input("Depth (or elevation): "))
	comment = input("Enter comment: ")

	# Report results
	try:
		position = subnautical.resolve(station_a_distance, station_a, station_b_distance, station_b, depth=depth)
		response = f'{comment} located at {position}'
		print(response)
		logging.info(response)
	except subnautical.ImpossiblePositionError as err:
		print(err)

	input() # Wait for input to clear the screen