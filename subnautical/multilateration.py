import math

class ImpossiblePositionError(Exception):
	pass

def solve_for_baseleg(hypotenuse, a):
	'''
	Returns the length of the base leg of a right triangle given the hypotenuse and side a (the depth/elevation).
	'''
	baseleg = math.sqrt(
		math.pow(hypotenuse, 2) - math.pow(a, 2)
	)
	return baseleg

def solve_x(r1, r2, c1, c2):
	# solve the numerator
	# r1 squared - r2 squared + U squared
	numerator = math.pow(r1, 2) - math.pow(r2, 2) + math.pow(c2['x'], 2)
	# solve the denominator
	denominator = 2 * c2['x']

	return numerator/denominator

def solve_y(r1, x):
	return math.sqrt(
		math.pow(r1, 2) - math.pow(x, 2)
	)

def resolve(distance1, station1, distance2, station2, depth=None):
	position = {
		'x': None,
		'y': None,
		'z': None
		}

	if depth is not None:
		distance1 = solve_for_baseleg(distance1, depth - station1['z'])
		distance2 = solve_for_baseleg(distance2, depth - station2['z'])
		position['z'] = depth

	# add an offset in situations where we'd get a mathematical error
	# this is solved by transforming the x coordinates so that station 1 is at 0 on the x axis, then reversing that offset before returning the resulting position
	offset_x = station1['x']
	station1['x'] -= offset_x
	station2['x'] -= offset_x

	position['x'] = solve_x(distance1, distance2, station1, station2)
	try:
		position['y'] =	solve_y(distance1, position['x'])
	except ValueError as err:
		raise ImpossiblePositionError("The radials between these stations do not intersect.")

	position['x'] += offset_x
	return position